//
//  MarketPlaceDTO.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarketPlaceDTO : NSObject

@property (nonatomic, retain) NSNumber * marketPlaceCode;
@property (nonatomic, retain) NSString * marketPlace;
@property (nonatomic, retain) NSString * marketPlaceDescription;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSString * searchUrl;

@end
