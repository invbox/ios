//
//  SearchResultsTableViewController.m
//  iInventNSell
//
//  Created by user on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchResultsTableViewController.h"

static NSString *kDeleteAllTitle = @"Delete All";
static NSString *kDeletePartialTitle = @"Delete (%d)";


@implementation SearchResultsTableViewController
@synthesize categories;
@synthesize searchItem;
@synthesize searchResults;
@synthesize resultsLoaded;
@synthesize activityIndicator;
@synthesize viewController, editButton, cancelButton, deleteButton, addButton;

- (IBAction)done:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}


- (void)awakeFromNib
{
    [self.tableView setBackgroundColor:kVerticalTableBackgroundColor];
    self.tableView.rowHeight = kCellHeight + (kRowVerticalPadding * 0.5) + ((kRowVerticalPadding * 0.5) * 0.5);
}




- (void) reload {
    [[self tableView] reloadData];
}

- (void)resetUI
{
    // leave edit mode for our table and apply the edit button
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = self.editButton;
    [self.tableView setEditing:NO animated:YES];
    
    self.navigationItem.leftBarButtonItem = self.addButton;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self tableView].backgroundColor = [UIColor colorWithRed:242/255.0f green:207/255.0f blue:48/255.0f alpha:1];
    UIImage* background = [UIImage imageNamed:@"coppersulphatebackground.jpeg"];
    self.navigationController.view.backgroundColor = [UIColor colorWithPatternImage:background];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor colorWithPatternImage:background];
    NSLog(@"View loaded .............");
    UIImageView *boxBackView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"coppersulphatebackground.jpeg"]];
    [self.tableView setBackgroundView:boxBackView];
    [[UITabBar appearance] setBackgroundImage:background];
    
}

- (void) initializeSearchRequest
{
    int width = self.view.frame.size.width;
    int height = self.view.frame.size.height;
    NSLog(@"width is %d", width);
    NSLog(@"height is %d", height);
    width = width/2;
    height = height/2;
    NSLog(@"width is %d", width);
    NSLog(@"height is %d", height);
    
    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithFrame:CGRectMake(width, height, 10.0f, 10.0f)];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    [activityIndicator setHidesWhenStopped:YES];
    
    [self setResultsLoaded:FALSE];
    
    OrganicSearch *organicSearch = [[OrganicSearch alloc] init];
    [organicSearch initializeSearchRequest:searchItem forTableView:self];
    
    
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.deleteButton.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = self.editButton;
    UIColor *color = [UIColor colorWithRed:117/255.0f green:4/255.0f blue:32/255.0f alpha:1];
    self.navigationController.toolbar.tintColor = color;
    self.navigationController.toolbar.tintColor = [UIColor blackColor];
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
        
    self.searchResults = nil;
    self.viewController = nil;
        
    self.editButton = nil;
    self.cancelButton = nil;
    self.deleteButton = nil;
    self.addButton = nil;
}

- (void)dealloc
{
    [searchResults release];
    [viewController release];
    
    [editButton release];
    [cancelButton release];
    [deleteButton release];
    [addButton release];
    
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initializeSearchRequest];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!resultsLoaded)
        return 0;
    else
        //return [searchResults count];
    return 1;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.tableView.isEditing)
    {
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        self.deleteButton.title = (selectedRows.count == 0) ?
        kDeleteAllTitle : [NSString stringWithFormat:kDeletePartialTitle, selectedRows.count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"index path section %d",indexPath.section );
    
    if(indexPath.section==1)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"HorizontalCell";
    
        HorizontalTableCell *cell = (HorizontalTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
        if (cell == nil)
        {
            cell = [[[HorizontalTableCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)] autorelease];
        }
    
        SearchItem *item = [searchResults objectAtIndex:[indexPath row]];
        cell.articles = searchResults;
        cell.searchResultsController=self;
        return cell;
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];*/
    SearchItem *item = [searchResults objectAtIndex:[indexPath row]];
    
    if (!self.tableView.isEditing)
    {
        self.viewController.title = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
        ItemDetailsViewController *viewController = [[self storyboard]instantiateViewControllerWithIdentifier:@"ItemDetailsViewController"];
        [viewController setNavigatingFrom:@"ItemDetails"];
        [viewController setSearchItem:item];
        [[self navigationController] pushViewController:viewController animated:YES];
        //[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else
    {
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        NSString *deleteButtonTitle = [NSString stringWithFormat:kDeletePartialTitle, selectedRows.count];
        
        if (selectedRows.count == self.searchResults.count)
        {
            deleteButtonTitle = kDeleteAllTitle;
        }
        self.deleteButton.title = deleteButtonTitle;
    }
     
}


/*
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    // create and return a custom UIView to use for the section here
    NSString *imageName = [NSString alloc];
    
    switch (section) {
        case 0:
            imageName = @"googleprodsearchicon.jpeg";
            break;
        case 1:
            imageName= @"ebay.jpeg";
        default:
            break;
    }
    UIImage *image = [UIImage imageNamed:imageName];
    
    return [self createHeader:image];
    
}*/

- (UIView *) createHeader:(UIImage *)imageName
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 640, 150)];
    header.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    UIImageView *headerImage = [[UIImageView alloc] initWithImage:imageName];
    
    [header addSubview:headerImage];
    [headerImage release];
    return header;
}




- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = NSLocalizedString(@"Google", @"Google");
            break;
        case 1:
            sectionName = NSLocalizedString(@"eBay", @"eBay");
            break;
            // ...
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

#pragma mark -
#pragma mark Action methods

- (IBAction)editAction:(id)sender
{
    // setup our UI for editing
    self.navigationItem.rightBarButtonItem = self.cancelButton;
    
    self.deleteButton.title = kDeleteAllTitle;
    
    self.navigationItem.leftBarButtonItem = self.deleteButton;
    
    [self.tableView setEditing:YES animated:YES];
}

- (IBAction)cancelAction:(id)sender
{
    [self resetUI]; // reset our UI
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
        // delete the selected rows
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        if (selectedRows.count > 0)
        {
            // setup our deletion array so they can all be removed at once
            NSMutableArray *deletionArray = [NSMutableArray array];
            for (NSIndexPath *selectionIndex in selectedRows)
            {
                [deletionArray addObject:[self.searchResults objectAtIndex:selectionIndex.row]];
            }
            [self.searchResults removeObjectsInArray:deletionArray];
            
            // then delete the only the rows in our table that were selected
            [self.tableView deleteRowsAtIndexPaths:selectedRows withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else
        {
            [self.searchResults removeAllObjects];
            
            // since we are deleting all the rows, just reload the current table section
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        [self resetUI]; // reset our UI
        [self.tableView setEditing:NO animated:YES];
        
        self.editButton.enabled = (self.searchResults.count > 0) ? YES : NO;
    }
}

- (IBAction)deleteAction:(id)sender
{
    // open a dialog with just an OK button
    NSString *actionTitle = ([[self.tableView indexPathsForSelectedRows] count] == 1) ?
    @"Are you sure you want to remove this item?" : @"Are you sure you want to remove these items?";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:actionTitle
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view]; // show from our table view (pops up in the middle of the table)
    [actionSheet release];
}
- (void)insertSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation
{
    
}

- (IBAction)addAction:(id)sender
{
    [self.tableView beginUpdates]; 
    
    [self.searchResults addObject:@"New Item"];
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:(self.searchResults.count - 1) inSection:0]];
    [self.tableView insertRowsAtIndexPaths:paths withRowAnimation:NO];
    
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:(self.searchResults.count - 1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    self.editButton.enabled = (self.searchResults.count > 0) ? YES : NO;
}

@end
