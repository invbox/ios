//
//  OrganicSearch.h
//  InvBox
//
//  Created by Srikanth on 3/8/13.
//
//

#import <Foundation/Foundation.h>
#import "SearchItem.h"
#import "eBaySearchProvider.h"
#import "GoogleSearchProvider.h"


@class SearchResultsTableViewController;

@interface OrganicSearch : NSObject
{
    NSString *searchString;
    NSMutableArray *searchResults;
    NSMutableData *responseData;
    SearchResultsTableViewController *searchResultsController;
    NSOperationQueue *searchQueue;
}

@property (nonatomic, retain) NSMutableArray *searchResults;
@property (nonatomic, retain) NSString *searchString;
@property (nonatomic, retain) NSMutableData *responseData;
extern NSInteger const MAX_SEARCH_RESULTS;
extern NSString * const GOOGLE_SEARCH_STRING;
extern NSString * const EBAY_SEARCH_STRING;
extern NSString * const GOOOGLE_KEY;
extern NSString * const EBAY_KEY;

@property (nonatomic, retain) SearchResultsTableViewController *searchResultsController;
@property (nonatomic, retain) NSOperationQueue *searchQueue;
- (void) initializeSearchRequest:(NSString *)searchValue forTableView:(SearchResultsTableViewController *)searchController;
- (void) dataLoaded:(NSMutableArray *)searchResults;
@end
