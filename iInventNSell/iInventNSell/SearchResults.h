//
//  SearchResults.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item, UserProfile;

@interface SearchResults : NSManagedObject

@property (nonatomic, retain) NSNumber * searchResultCode;
@property (nonatomic, retain) NSNumber * listPrice;
@property (nonatomic, retain) Item *item;
@property (nonatomic, retain) UserProfile *userProfile;

@end
