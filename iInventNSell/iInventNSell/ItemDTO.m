//
//  ItemData.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ItemDTO.h"

@implementation ItemDTO

@synthesize creationDate;
@synthesize itemCode;
@synthesize itemDescription;
@synthesize itemName;
@synthesize purchasePrice;
@synthesize category;
@synthesize userProfile;
@synthesize imageURL;
@synthesize conditionType;
@synthesize source;
@synthesize currency;
@synthesize availability;
@synthesize type;

@end
