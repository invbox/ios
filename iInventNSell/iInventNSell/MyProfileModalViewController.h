//
//  MyProfileModalViewController.h
//  iInventNSell
//
//  Created by user on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iInventNSellAppDelegate.h"
#import "UserProfileDTO.h"
#import "ControlVariables.h"

@interface MyProfileModalViewController : UIViewController {
    UITextField *profileCode;
    UITextField *firstName;
    UITextField *lastName;
    UITextField *emailId;    
    UISwitch *retainHistory;
    UITextField *lastModified;
    UserProfileDTO *userProfileDTO;
    UserProfile *userProfile;
    UITextField *textField;
}
    @property (nonatomic, retain) IBOutlet UITextField *profileCode;
    @property (nonatomic, retain) IBOutlet UITextField *firstName;
    @property (nonatomic, retain) IBOutlet UITextField *lastName;
    @property (nonatomic, retain) IBOutlet UITextField *emailId;
    @property (nonatomic, retain) IBOutlet UITextField *textField;
    @property (nonatomic, retain) IBOutlet UISwitch *retainHistory;
    @property (nonatomic, retain) UserProfileDTO *userProfileDTO;
    @property (nonatomic, retain) IBOutlet UITextField *lastModified;
    @property (nonatomic, retain) UserProfile *userProfile;

- (IBAction)done:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)saveAndClose:(id)sender;
@end
