//
//  PreferencesTableViewController.h
//  iInventNSell
//
//  Created by user on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iInventNSellAppDelegate.h"
#import "DataController.h"
#import "MarketPlace.h"
#import "ControlVariables.h"

@interface PreferencesTableViewController : UITableViewController
{
    NSMutableArray *dataArray;
    NSMutableArray *searchRepositories;
}

- (IBAction)done:(id)sender;
@property (nonatomic, retain) NSMutableArray *dataArray;
@property (nonatomic, retain) NSMutableArray *searchRepositories;
@end
