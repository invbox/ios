//
//  UserPreferencesDTO.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MarketPlaceDTO.h"
#import "UserProfileDTO.h"

@interface UserPreferencesDTO : NSObject

@property (nonatomic, retain) NSNumber * preferenceCode;
@property (nonatomic ) Boolean marketPlaceEnabled;
@property (nonatomic, retain) MarketPlaceDTO *marketPlace;
@property (nonatomic, retain) UserProfileDTO *userProfile;


@end
