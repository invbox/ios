//
//  DataController.m
//  iInventNSell
//
//  Created by user on 4/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataController.h"
#import "iInventNSellAppDelegate.h"

@implementation DataController
@synthesize managedObjectContext;

- (void) initializeDemoData {

    [self createDemoCategories];
    [self createDemoUser];
    [self createDemoMarketPlaceProviders];
    [self createDemoUserPreferences];
    [self createDemoItems];
}

- (void) createDemoUser {
    UserProfileDTO *userProfileDTO = [[UserProfileDTO alloc] init];
    [userProfileDTO setFirstName:@"Jason"];
    [userProfileDTO setLastName:@"Bourne"];
    [userProfileDTO setEmail:@"jbourne@ludlum.com"];
    [userProfileDTO setUserProfileCode:[[NSNumber alloc]initWithInt:1]];
    [userProfileDTO setRetainHistory:[[NSNumber alloc]initWithInt:1]];
    [userProfileDTO setCreationDate:[NSDate date]];
    [self createUser:userProfileDTO];
}

- (void) createDemoItems;
{
    ItemDTO *itemDTO = [[ItemDTO alloc]init];
    [itemDTO setItemCode:[NSNumber numberWithInt:1]];
    [itemDTO setItemName:@"iPhone 4"];
    [itemDTO setItemDescription:@"Revolutionary iPhone 4 is here"];
    [itemDTO setPurchasePrice:[NSNumber numberWithDouble:499.99]];
    [self createItem:itemDTO]; 
}
- (void) createDemoInventory {
    
}

- (void) createDemoSellList {
    
}

- (void) createDemoBuyList {

}

- (void) createDemoHistory {
   // @property (nonatomic, retain) NSManagedObject *searchresults;
   // @property (nonatomic, retain) UserProfileDTO *userprofile;

    
    HistoryDTO *historyDTO = [[HistoryDTO alloc]init];
    [historyDTO setHistoryCode:[[NSNumber alloc]initWithInt:1]];
    [historyDTO setSearchDate:[NSDate date]];
    
    [self createMarketPlace:historyDTO];
    [historyDTO release];    
}
    
- (void) createDemoMarketPlaceProviders {
    MarketPlaceDTO *marketPlaceDTO = [[MarketPlaceDTO alloc]init];
    [marketPlaceDTO setMarketPlaceCode:[[NSNumber alloc]initWithInt:1]];
    [marketPlaceDTO setMarketPlace:@"Amazon"];
    [marketPlaceDTO setMarketPlaceDescription:@"A to Z"];
    [marketPlaceDTO setEmail:@"developer@amazon.com"];
    [marketPlaceDTO setSearchUrl:@"www.amazon.com/search/a"];
    [marketPlaceDTO setWebsite:@"www.amazon.com"];
    [self createMarketPlace:marketPlaceDTO];    
    [marketPlaceDTO release];
    
    marketPlaceDTO = [[MarketPlaceDTO alloc] init];
    [marketPlaceDTO setMarketPlaceCode:[[NSNumber alloc]initWithInt:2]];
    [marketPlaceDTO setMarketPlace:@"ebay"];
    [marketPlaceDTO setMarketPlaceDescription:@"Everything under the Sun"];
    [marketPlaceDTO setEmail:@"developer@ebay.com"];
    [marketPlaceDTO setSearchUrl:@"www.ebay.com/search/a"];
    [marketPlaceDTO setWebsite:@"www.ebay.com"];
    [self createMarketPlace:marketPlaceDTO];
    //@"Amazon",@"eBay",@"BestBuy", @"Deals2Buy", @"Google Product Search", @"SearchUPC", nil]
    
    [marketPlaceDTO release];

    marketPlaceDTO = [[MarketPlaceDTO alloc] init];
    [marketPlaceDTO setMarketPlaceCode:[[NSNumber alloc]initWithInt:3]];
    [marketPlaceDTO setMarketPlace:@"BestBuy"];
    [marketPlaceDTO setMarketPlaceDescription:@"Best deals everyday!"];
    [marketPlaceDTO setEmail:@"developer@bestbuy.com"];
    [marketPlaceDTO setSearchUrl:@"www.bestbuy.com/search/a"];
    [marketPlaceDTO setWebsite:@"www.bestbuy.com"];
    [self createMarketPlace:marketPlaceDTO];
    [marketPlaceDTO release];

    marketPlaceDTO = [[MarketPlaceDTO alloc] init];
    [marketPlaceDTO setMarketPlaceCode:[[NSNumber alloc]initWithInt:4]];
    [marketPlaceDTO setMarketPlace:@"Deals2Buy"];
    [marketPlaceDTO setMarketPlaceDescription:@"Deals 2 buy"];
    [marketPlaceDTO setEmail:@"developer@deals2buy.com"];
    [marketPlaceDTO setSearchUrl:@"www.deals2buy.com/search/a"];
    [marketPlaceDTO setWebsite:@"www.deals2buy.com"];
    [self createMarketPlace:marketPlaceDTO];
    [marketPlaceDTO release];

    marketPlaceDTO = [[MarketPlaceDTO alloc] init];
    [marketPlaceDTO setMarketPlaceCode:[[NSNumber alloc]initWithInt:5]];
    [marketPlaceDTO setMarketPlace:@"Google Product Search"];
    [marketPlaceDTO setMarketPlaceDescription:@"Find best deals across"];
    [marketPlaceDTO setEmail:@"developer@googleproductsearch.com"];
    [marketPlaceDTO setSearchUrl:@"www.google.com/search/a"];
    [marketPlaceDTO setWebsite:@"www.google.com"];
    [self createMarketPlace:marketPlaceDTO];
    
}

- (void) createDemoUserPreferences {
    UserPreferencesDTO *prefs = [[UserPreferencesDTO alloc]init];
    [prefs setPreferenceCode:[[NSNumber alloc] initWithInt:1]];
    [self createUserPreferences:prefs];    
}


- (void) createDemoCategories {
    
    NSString * categoryName = @"Computers";
    NSString * categoryDescription = @"Computers, Laptops, Accessories etc.,";
    [self createNewCategoryWithName:categoryName andDescription:categoryDescription];
    
    categoryName = @"Electronics";
    categoryDescription = @"Gadgets, peripherals etc.,";
    [self createNewCategoryWithName:categoryName andDescription:categoryDescription];
    
    categoryName = @"Mobiles";
    categoryDescription = @"Mobiles, cases, pouches etc.,";
    [self createNewCategoryWithName:categoryName andDescription:categoryDescription];
    
    categoryName = @"Tablets";
    categoryDescription = @"Tablet pcs, netbooks etc.,";
    [self createNewCategoryWithName:categoryName andDescription:categoryDescription];
    
    categoryName = @"Garments";
    categoryDescription = @"Clothes, textiles etc.,";
    [self createNewCategoryWithName:categoryName andDescription:categoryDescription];
    
    categoryName = @"Books";
    categoryDescription = @"Best selling books, childrens summer special etc.,";
    [self createNewCategoryWithName:categoryName andDescription:categoryDescription];
    
    categoryName = @"Household";
    categoryDescription = @"House hold, kitchen ware etc.,";
    [self createNewCategoryWithName:categoryName andDescription:categoryDescription];
}

- (void) deleteObject:(id)object {
     [managedObjectContext deleteObject:object];
    NSError *error;
    [managedObjectContext save:&error];
}

- (NSArray *)getAllRecordsFromDB:(NSString *)entity {
	
	NSEntityDescription *entityDescription = [NSEntityDescription entityForName:entity inManagedObjectContext:managedObjectContext];
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity: entityDescription];
	
	NSError *error = nil;
	NSArray *results = [managedObjectContext executeFetchRequest:request error:&error];
	
	if (!results || error) {
		NSLog(@"[ERROR] COREDATA: Fetch request raised an error - '%@'", [error description]);
		return nil;
	}
	
	[request release];
    return results;
}

- (void) createUserPreferences:(UserPreferencesDTO *)userPreferences {
    UserPreferences  *prefs = [NSEntityDescription insertNewObjectForEntityForName:@"UserPreferences" inManagedObjectContext:managedObjectContext];
    
    
    NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
	
    NSLog(@"[SUCCESS] COREDATA: Inserted new user to database!");    
}

- (void) createMarketPlace:(MarketPlaceDTO *)marketPlaceDTO {
    MarketPlace  *marketPlace = [NSEntityDescription insertNewObjectForEntityForName:@"MarketPlace" inManagedObjectContext:managedObjectContext];
    
    [marketPlace setMarketPlaceCode:[marketPlaceDTO marketPlaceCode]];
    [marketPlace setMarketPlace:[marketPlaceDTO marketPlace]];
    [marketPlace setMarketPlaceDescription:[marketPlaceDTO marketPlaceDescription]];
    [marketPlace setEmail:[marketPlaceDTO email]];
    [marketPlace setWebsite:[marketPlaceDTO website]];
    [marketPlace setSearchUrl:[marketPlaceDTO searchUrl]];
    
    NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
	
    NSLog(@"[SUCCESS] COREDATA: Inserted new user to database!");
}

- (void)updateUser:(UserProfileDTO *)userProfileDTO forUserID:(NSNumber *)userProfileCode {
    //UserProfile *user = [self getUserProfileByUserProfileCode:userProfileCode];
    NSArray  *users = [self getAllRecordsFromDB:@"UserProfile"];
    UserProfile *user = [users objectAtIndex: 0];

	[user setFirstName:[userProfileDTO firstName]];
    [user setLastName:[userProfileDTO lastName]];
    [user setEmail:[userProfileDTO email]];
    [user setRetainHistory:[userProfileDTO retainHistory]];
    [user setPreferences:[userProfileDTO preferences]];
    [user setCreationDate:[userProfileDTO creationDate]];
    
	NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
		
	NSLog(@"[SUCCESS] COREDATA: Updated user in database!");
}


- (void) createUser:(UserProfileDTO *)userProfileDTO {
    UserProfile *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"UserProfile" inManagedObjectContext:managedObjectContext];
    //NSManagedObjectID *moID = [newUser objectID];
    [newUser setUserProfileCode:[userProfileDTO userProfileCode]];
    [newUser setFirstName:[userProfileDTO firstName]];
    [newUser setLastName:[userProfileDTO lastName]];
    [newUser setEmail:[userProfileDTO email]];
    [newUser setRetainHistory:[userProfileDTO retainHistory]];
    [newUser setPreferences:[userProfileDTO preferences]];
    [newUser setCreationDate:[userProfileDTO creationDate]];
    NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
	
    NSLog(@"[SUCCESS] COREDATA: Inserted new user to database!");
}

- (NSArray *)getItemsByType:(NSString *)type {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Item" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(type==%@)", type];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];

	
	if (!results || error) {
		NSLog(@"[ERROR] COREDATA: Fetch request raised an error - '%@'", [error description]);
		return nil;
	}
    
    [fetchRequest release];
    return results;
    
}

- (Item *) getItemByItemCode:(NSNumber *)itemCode {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Item" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(itemCode==%d)", itemCode];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    int numRecords = [results count];
    if (numRecords==1)
        return (Item *)[results objectAtIndex:0];
    return nil;
}

- (void) deleteItem:(NSString *)objectId {
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSPersistentStoreCoordinator  *persistentStoreCoordinator = [appDelegate persistentStoreCoordinator]; 
    
    Item *item= (Item *)[managedObjectContext objectWithID:[persistentStoreCoordinator managedObjectIDForURIRepresentation:[NSURL URLWithString:objectId]]];
    
    if (item!=nil)
    {
        [managedObjectContext deleteObject:item];
        NSError *error;
        [managedObjectContext save:&error];

    }    
}

- (void) updateItem:(ItemDTO *)itemDTO withObjectId:(NSString *)objectId {
    
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSPersistentStoreCoordinator  *persistentStoreCoordinator = [appDelegate persistentStoreCoordinator]; 
    
    Item *item= (Item *)[managedObjectContext objectWithID:[persistentStoreCoordinator managedObjectIDForURIRepresentation:[NSURL URLWithString:objectId]]];
    
    if (item==nil)
    {
        NSLog(@"COREDATA: Item not found in the repository");
        return;
    }
    
    
    [item setItemCode:[itemDTO itemCode]];
    [item setItemName:[itemDTO itemName]];
    [item setItemDescription:[itemDTO itemDescription]];
    [item setPurchasePrice:[itemDTO purchasePrice]];
    [item setCategory:[itemDTO category]];
    [item setSource:[itemDTO source]];
    [item setImageURL:[itemDTO imageURL]];
    [item setCurrency:[itemDTO currency]];
    [item setAvailability:[itemDTO availability]];
    [item setConditionType:[itemDTO conditionType]];
    
	NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
    
	NSLog(@"[SUCCESS] COREDATA: Updated user in database!");
    
    
    
}

- (void) createItem:(ItemDTO *)itemDTO {
    //NSNumber *itemCode = [[NSNumber alloc] initWithInt:1];
    Item *item = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:managedObjectContext];
    NSLog(@"Item description:%@",[itemDTO itemDescription]);
    [item setItemCode:[itemDTO itemCode]];
    [item setItemName:[itemDTO itemName]];
    [item setItemDescription:[itemDTO itemDescription]];
    [item setPurchasePrice:[itemDTO purchasePrice]];
    [item setCategory:[itemDTO category]];
    [item setSource:[itemDTO source]];
    [item setImageURL:[itemDTO imageURL]];
    [item setCurrency:[itemDTO currency]];
    [item setAvailability:[itemDTO availability]];
    [item setConditionType:[itemDTO conditionType]];
    
    NSError *error = nil;
	[managedObjectContext save:&error];
	NSString *objectId=[[[item objectID] URIRepresentation] absoluteString];
    NSLog(@"object id %@",objectId );
    
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
	
    NSLog(@"[SUCCESS] COREDATA: Inserted new category to database!");
}


- (void) createNewItemWithName:(NSString *)itemName andDescription:(NSString *)itemDescription {
    NSNumber *itemCode = [[NSNumber alloc] initWithInt:1];
    Item *item = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:managedObjectContext];
    [item setItemCode:itemCode];
    [item setItemName:itemName];
    [item setItemDescription:itemDescription];
    
    NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
    NSLog(@"[SUCCESS] COREDATA: Inserted new category to database!");
}

- (void)createCategory:(CategoryDTO *)categoryDTO  {
	//NSNumber *categoryCode = [[NSNumber alloc] initWithInt:1];
	Category *category = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:managedObjectContext];
	[category setCategoryCode:[categoryDTO categoryCode]];
	[category setCategoryName: [categoryDTO categoryName]];
	[category setCategoryDescription: [categoryDTO categoryDescription]];
	NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
	
	//[Category addObject: category];
    NSLog(@"[SUCCESS] COREDATA: Inserted new category to database!");
}


- (void)createNewCategoryWithName:(NSString *)arg_name andDescription:(NSString *)arg_description {
	NSNumber *categoryCode = [[NSNumber alloc] initWithInt:1];
	Category *category = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:managedObjectContext];
	[category setCategoryCode:categoryCode];
	[category setCategoryName: arg_name];
	[category setCategoryDescription: arg_description];
	
	NSError *error = nil;
	[managedObjectContext save:&error];
	
	if (error) {
		NSLog(@"[ERROR] COREDATA: Save raised an error - '%@'", [error description]);
		return;
	}
	
	//[Item addObject: item];
    NSLog(@"[SUCCESS] COREDATA: Inserted new item to database!");
}

- (NSArray *) getHistory {
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"History" inManagedObjectContext:managedObjectContext];
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	[request setEntity: entityDescription];
	
	NSError *error = nil;
	NSArray *results = [managedObjectContext executeFetchRequest:request error:&error];
	
	if (!results || error) {
		NSLog(@"[ERROR] COREDATA: Fetch request raised an error - '%@'", [error description]);
		return nil;
	}
	
	[request release];
    return results;
}


- (UserProfile *) getUserProfileByUserProfileCode:(NSNumber *)userProfileCode {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"UserProfile" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(userProfileCode==%d)", userProfileCode];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    NSNumber *numRecords = [results count];
    if (numRecords==1)
        return (UserProfile *)[results objectAtIndex:0];
    return nil;
}

//- (NSArray *) categories:(NSString *)categoryName {
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
//    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:managedObjectContext];
//    
//    [fetchRequest setEntity:entityDescription];
//    if (categoryName!=nil && [categoryName length]>0) {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(categoryName==%@)", categoryName];
//        [fetchRequest setPredicate:predicate];
//        
//    }
//    
//    NSError *error;
//    NSArray * results = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
//    [fetchRequest release];
//    return results;                                              
//}

- (void) deleteCategory:(Category *)category {
    [managedObjectContext deleteObject:category];
    NSError *error;
    [managedObjectContext save:&error];
}

@end
