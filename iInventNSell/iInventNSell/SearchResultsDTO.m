//
//  SearchResultsDTO.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchResultsDTO.h"

@implementation SearchResultsDTO

@synthesize searchResultCode;
@synthesize listPrice;
@synthesize item;
@synthesize userProfile;

@end
