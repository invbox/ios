//
//  DataController.h
//  iInventNSell
//
//  Created by user on 4/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Category.h"
#import "Item.h"
#import "UserProfile.h"
#import "UserPreferences.h"
#import "SearchResults.h"
#import "History.h"
#import "BuyList.h"
#import "MarketPlace.h"
#import "SellList.h"

#import "CategoryDTO.h"
#import "ItemDTO.h"
#import "UserProfileDTO.h"
#import "UserPreferencesDTO.h"
#import "SearchResultsDTO.h"
#import "HistoryDTO.h"
#import "BuyListDTO.h"
#import "MarketPlaceDTO.h"
#import "SellListDTO.h"


@interface DataController : NSObject{
    NSManagedObjectContext *managedObjectContext;
}

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

#pragma mark - Initialization methods
- (void) initializeDemoData;
- (void) createDemoCategories;
- (void) createDemoItems;
- (void) createDemoInventory;
- (void) createDemoSellList;
- (void) createDemoBuyList;
- (void) createDemoHistory;
- (void) createDemoMarketPlaceProviders;
- (void) createDemoUserPreferences;
- (void) createDemoUser;

#pragma mark - Generic methods
- (NSArray *)getAllRecordsFromDB:(NSString *)entity;
- (NSArray *)getItemsByType:(NSString *)type;
- (void) deleteObject:(id)object;

# pragma mark - Market Place operations
- (void) createMarketPlace:(MarketPlaceDTO *)marketPlace;
- (void) createUserPreferences:(UserPreferencesDTO *)userPreferences;

#pragma mark - User management operations
//- (void) createNewUser;
- (void) createUser:(UserProfileDTO *)userProfileDTO;
- (void)updateUser:(UserProfileDTO *)userProfileDTO forUserID:(NSNumber *)userProfileCode;
- (UserProfile *) getUserProfileByUserProfileCode:(NSNumber *)userProfileCode;
    
# pragma mark - Category management operations
- (void) createCategory:(CategoryDTO *)categoryDTO;
- (void) createNewCategoryWithName:(NSString *)categoryName andDescription:(NSString *) categoryDescription;
- (void) deleteCategory:(Category *)category;


# pragma mark - Item management operations
- (void) createNewItemWithName:(NSString *)itemName andDescription:(NSString *) itemDescription;
- (void) createItem:(ItemDTO *)itemDTO ;
- (void) updateItem:(ItemDTO *)itemDTO withObjectId:(NSString *) objectId;
- (void) deleteItem:(NSString *)objectId;
- (Item *) getItemByItemCode:(NSNumber *)itemCode;

- (NSArray *) getHistory;
//- (NSArray *) filterCategoriesWithCategoryName:(NSString *)categoryName;
//- (NSArray *) categories;
//- (Category *) addCategoryWithName:(NSString *)categoryName andDescription:(NSString *) categoryDescription;
//- (NSArray *) getAllCategoriesFromDB;
@end
