//
//  eBaySearchProvider.m
//  InvBox
//
//  Created by Srikanth on 3/11/13.
//
//

#import "eBaySearchProvider.h"

@implementation eBaySearchProvider

@synthesize searchResults;
@synthesize searchString;
@synthesize responseData;


NSString * const EBAY_SEARCH_URL=@"http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&GLOBAL-ID=EBAY-US&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD=true&paginationInput.entriesPerPage=10";
NSString * const EBAY_APP_KEY=@"Cordysbdb-4c22-430f-9779-d37db46a34e";

- (void)main {
    // a lengthy operation
    @autoreleasepool {
        // is this operation cancelled?
        if (self.isCancelled)
            return;
        
        if (searchString==nil)
        {
            NSLog(@"searchItem is null, returning");
            return;
        }
        
        NSLog(@"Organic search called with search Parameter: %@", searchString);
        /*NSString *url = @"https://www.googleapis.com/shopping/search/v1/public/products?key=AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA&country=US&q=";*/
        NSString *url = EBAY_SEARCH_URL;
        if (self.isCancelled)
            return;
        
        //keywords=harry%20potter
        //url = [url stringByAppendingString:MAX_SEARCH_RESULTS];
        url = [url stringByAppendingString:@"&keywords="];
        url = [url stringByAppendingString:searchString];
        url = [url stringByAppendingString:@"&SECURITY-APPNAME="];
        url = [url stringByAppendingString:EBAY_APP_KEY];
        if (self.isCancelled)
            return;
        
        NSLog(@"URL Constructed is %@", url);
        responseData = [[NSMutableData data] retain];
        if (self.isCancelled)
            return;
        
        searchResults = [NSMutableArray array];
        if (self.isCancelled)
            return;
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        [connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
//        [(NSObject *)self.delegate performSelectorOnMainThread:(@selector(searchComplete:)) withObject:object waitUntilDone:NO];
        [connection start];

    }
}

/*ebay
 http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0
 &SECURITY-APPNAME=Cordysbdb-4c22-430f-9779-d37db46a34e
 &GLOBAL-ID=EBAY-US
 &RESPONSE-DATA-FORMAT=JSON
 &REST-PAYLOAD
 &keywords=harry%20potter
 &paginationInput.entriesPerPage=10
 
 
 
 appid: Cordysbdb-4c22-430f-9779-d37db46a34e
 url:http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0
 key:Cordysbdb-4c22-430f-9779-d37db46a34e
 callname: method invoking like FindItemsByKeyWords
 Search String: QueryKeywords, ex ipod
 */


/*
 Google
 url: "https://www.googleapis.com/shopping/search/v1/public/products?key=AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA&country=US&q=ipad";
 callname: products, params key, country=us and query string
 key:AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA
 
 
 */
- (void) initializeSearchRequest:(NSString *)searchValue andOrganicSearch:(OrganicSearch *) organicSearch
{
    self.searchString=searchValue;
    self.organicSearch = organicSearch;
}


#pragma mark NSURLConnection Delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Connection failed: %@", [error description]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    @try {
        
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(responseString);
        NSLog(@"Connection did finish loading\n\n\n\n");
        //  NSDictionary *results = [responseString JSONValue];
        //parse out the json data

        NSError* error;
        
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:responseData //1
                              options:kNilOptions
                              error:&error];

        NSArray* results = [json objectForKey:@"item"]; //2
        //NSDictionary *resp = [json ]
        NSLog(@"Items: %@", results); //3
        NSDictionary *response = [results objectAtIndex:0];
        NSMutableArray *resultItems = [response objectForKey:@"itemId"];
        /*NSDictionary *response = [responseString JSONValue];
        NSMutableArray *results = [response objectForKey:@"searchResult"];
        searchResults = [[NSMutableArray alloc]init];
        */
        for (NSDictionary *item  in results)
        {
            SearchItem *searchItem = [[SearchItem alloc]init];
            //NSDictionary *product = [item  objectForKey:@"product"];
            [searchItem setTitle:[item objectForKey:@"title"]];
            [searchItem setDescription:[item objectForKey:@"description"]];
            [searchItem setConditionType:[item objectForKey:@"condition"]];
            NSArray *images = [item objectForKey:@"images"];
            NSDictionary *linkImage = [images objectAtIndex:0];
            NSString *stringurl = [linkImage objectForKey:@"link"];
            NSURL *url = [NSURL URLWithString:stringurl];
            [searchItem setImageURL:url];
            
            //[searchItem setCategoryName:<#(NSString *)#>];
            
            NSArray *inventories = [item objectForKey:@"inventories"];
            NSDictionary *inventory = [inventories objectAtIndex:0];
            [searchItem setAvailability:[inventory objectForKey:@"availability"]];
            [searchItem setCurrency:[inventory objectForKey:@"currency"]];
            [searchItem setPrice:[inventory objectForKey:@"price"]];
            [searchItem setSource:@"Google Product Search"];
            [searchResults addObject:searchItem];
            if ([searchResults count]==10) {
                break;
            }
        }
        
        NSLog(@"After setting search results total items are: %d" , [searchResults count]);
        //[[self organicSearch] dataLoaded:searchResults];
    }
    @catch (NSException *exception) {
        NSLog(@"**********\n\nAn exception has occured\n\n******************" && [exception name]);
    }
    @finally {
        [connection release];
        [responseData release];
    }
}



@end
