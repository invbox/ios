//
//  ItemDetailsViewController.m
//  iInventNSell
//
//  Created by user on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ItemDetailsViewController.h"
#import "ItemDTO.h"
#import "DataController.h"
#import "iInventNSellAppDelegate.h"

@implementation ItemDetailsViewController
@synthesize availability;
@synthesize title;
@synthesize productImage;
@synthesize description;
@synthesize price;
@synthesize searchItem;
@synthesize  navigatingFrom;
@synthesize addToInventory;
@synthesize purchasePrice;
@synthesize editButton;
@synthesize saveButton;
@synthesize sellButton;
Boolean editClicked;

- (IBAction)edit:(id) sender
{


    if (editClicked==TRUE)
    {
        [self.purchasePrice setHidden:FALSE];
        saveButton.hidden = FALSE;
        editButton.hidden = TRUE;
        editClicked = FALSE;
    }
    else
    {
        [self.purchasePrice setHidden:TRUE];
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber * myNumber = [f numberFromString:purchasePrice.text];
        [f release];
        [searchItem setPrice:myNumber];
        [price setText:purchasePrice.text];
        saveButton.hidden = TRUE;
        editButton.hidden = FALSE;
        editClicked = TRUE;
        [self updateItem];
    }
}

- (IBAction)editingDone {
    [self.view.window endEditing:TRUE];
    [purchasePrice resignFirstResponder]; // if you want the keyboard to go away
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (void)updateItem {
    NSLog(@"update to inventory clicked");
    ItemDTO *itemDTO = [[ItemDTO alloc]init];
    NSLog(@"Item description:%@", [searchItem description]);
    NSLog(@"Pircie: %d", [searchItem price]);
    [itemDTO setItemCode:[NSNumber numberWithInt:1]];
    [itemDTO setItemName:searchItem.title];
    [itemDTO setItemDescription:searchItem.description];
    [itemDTO setPurchasePrice:[searchItem price]];
    [itemDTO setCategoryCode:[NSNumber numberWithInt:1]];
    [itemDTO setImageURL:searchItem.imageURL];
    [itemDTO setCurrency:searchItem.currency];
    [itemDTO setAvailability:searchItem.availability];
    [itemDTO setConditionType:searchItem.conditionType];
    
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController]; 
    [dataController updateItem:itemDTO withObjectId:searchItem.objectId];
    
}

- (void) addToHistory {
    
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController];
    

    UserProfile *userProfile = [dataController getUserProfileByUserProfileCode:1];
    
    History   *history = [NSEntityDescription insertNewObjectForEntityForName:@"History" inManagedObjectContext:[dataController managedObjectContext]];
    
    //HistoryDTO *historyDTO = [[HistoryDTO alloc]init];
    [history setSearchDate:[NSDate date]];
    [history setUserProfile:userProfile];
    
    SearchResults  *searchResults = [NSEntityDescription insertNewObjectForEntityForName:@"SearchResults" inManagedObjectContext:[dataController managedObjectContext]];
    
    [searchResults setSearchResultCode:[NSNumber numberWithInt:1]];
    [searchResults setListPrice:[NSNumber numberWithInt:1]];
    
   
    
    Item *item = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:[dataController managedObjectContext]];
    [item setItemCode:[NSNumber numberWithInt:1]];
    [item setItemName:searchItem.title];
    [item setItemDescription:searchItem.description];
    [item setPurchasePrice:searchItem.price];
    [item setImageURL:searchItem.imageURL];
    [item setCurrency:searchItem.currency];
    [item setAvailability:searchItem.availability];
    [item setConditionType:searchItem.conditionType];
    [item setType:@"history"];
    [searchResults setItem:item];
    [searchResults setUserProfile:userProfile];
    
    [history setSearchResults:searchResults];
    
    NSError *error = nil;
    if (![[dataController managedObjectContext] save:&error]) {
        NSLog(@"Error in adding a new bank %@, %@", error, [error userInfo]);
        abort();
    }
    //to create history, searchresult, item, userprofile
    
}

- (IBAction)addToInventory:(id)sender
{
   NSLog(@"Button pressed: %@", [sender currentTitle]);
    [self saveToServer:searchItem forType:[sender currentTitle]];
    /*
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController];
    UserProfile *userProfile = [dataController getUserProfileByUserProfileCode:1];
    
    Item *item = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:[dataController managedObjectContext]];
    
    [item setItemName:searchItem.title];
    [item setItemDescription:searchItem.description];
    [item setPurchasePrice:searchItem.price];
    [item setImageURL:searchItem.imageURL];
    [item setCurrency:searchItem.currency];
    [item setAvailability:searchItem.availability];
    [item setConditionType:searchItem.conditionType];
    [item setUserProfile:userProfile];
    
    if ([[sender currentTitle]isEqual:@"Sell"])
    {
        [item setType:@"Sell"];
    }
    else if ([[sender currentTitle]isEqual:@"Buy"])
    {
        [item setType:@"Buy"];
    }
    
    
    
    NSError *error = nil;
    if (![[dataController managedObjectContext] save:&error]) {
        NSLog(@"Error in adding a new bank %@, %@", error, [error userInfo]);
        abort();
    }
    
    NSLog(@"List item saved");
    //to create history, searchresult, item, userprofile
    */
    
}

- (void) saveToServer:(SearchItem *)item forType:(NSString *)type
{
    NSLog(@"item code:%@",item.itemCode);
    NSLog(@"item name:%@",item.title);
    NSLog(@"item desc:%@", item.description);
    NSLog(@"item price:%@", item.price);
    NSLog(@"item type:%@", type);
    NSString *post = [NSString stringWithFormat:@"&itemcode=%@&itemname=%@&itemdescription=%@&purchaseprice=%@&category.id=%@&profilecode=%@&status=%@&purchasedate_day=%@&purchasedate_month=%@&purchasedate_year=%@&type=%@",item.itemCode, item.title,item.description,item.price, @"1",@"1",@"new",@"27",@"July",@"2013",type];
//    NSString *post = [NSString stringWithFormat:@"&profileCode=%@&profileName=%@&firstname=%@&lastname=%@&password=%@&email=%@",@"5",@"Sah",@"Sahithi",@"K",@"sahithi",@"sahithi@gmail.com"];

    NSLog(@"post url is: %@",post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://garaze.elasticbeanstalk.com/item/save?requesttype=xml"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
    [request setHTTPBody:postData];
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];

    if(conn)
    {
        NSLog(@"Connection Successful");
    }
    else
    {
        NSLog(@"Connection could not be made");
    }

    
/*
http://garaze.elasticbeanstalk.com/invbox/item/save?requesttype=xml	Post	itemcode
    itemname
    itemdescription
    purchaseprice
    purchasedate
    purchasedate_day
    purchasedate_month
    purchasedate_year
    type
    image
    category.id
    create
    profilecode
    status
 */
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    NSLog(@"didReceiveData");
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
        NSLog(@"didFailWithError");
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
        NSLog(@"didFinishLoading");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addToHistory];
    //NSLog(@"descriptoin: ", [searchItem categoryName ]);
    NSLog(@"title: ", [searchItem title] );
    NSLog(@"Item description:%@", [searchItem description]);
    NSLog(@"product image " , [searchItem imageURL]);
    
    //self.description = searchItem.categoryName;
    self.title.text = [[NSString alloc] initWithFormat:@"%@", searchItem.title];
    NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    NSString *currencyString = [numberFormatter stringFromNumber:[searchItem price]];
    NSString *itemPrice = [[searchItem price] stringValue];
    NSString *cost = [[NSString alloc]initWithFormat:@"Price: %@", currencyString];
    self.price.text=cost;
        NSLog(@"price: ", cost);
    NSURL *url = searchItem.imageURL;
	NSData *data = [NSData dataWithContentsOfURL:url];
    self.description.text = [[NSString alloc]initWithFormat:@"%@", searchItem.description];
    
	self.productImage.image = [UIImage imageWithData:data];
    self.availability.text = [[NSString alloc] initWithFormat:@"Availability: %@", searchItem.availability];
    [self.purchasePrice setHidden:TRUE];
    [self.purchasePrice setText:itemPrice];
    saveButton.hidden=TRUE;
    editClicked = TRUE;
    if (self.navigatingFrom  ==@"Inventory")
    {
        self.addToInventory.enabled=NO;
        
    }
    else
    {
        self.addToInventory.enabled=YES;
    }
    
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage* background = [UIImage imageNamed:@"coppersulphatebackground.jpeg"];
    self.navigationController.view.backgroundColor = [UIColor colorWithPatternImage:background];
        [[UITabBar appearance] setBackgroundImage:background];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor blueColor];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
