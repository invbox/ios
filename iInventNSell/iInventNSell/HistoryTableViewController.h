//
//  HistoryTableViewController.h
//  iInventNSell
//
//  Created by user on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControlVariables.h"
#import "iInventNSellAppDelegate.h"
#import "DataController.h"
#import "SearchItem.h"

@interface HistoryTableViewController : UITableViewController
{
    NSMutableArray *historyItems;
	SearchItem *searchItem;
    NSIndexPath *currentIndex;
}
@property (nonatomic, retain) NSMutableArray *historyItems;
@property (nonatomic, retain) SearchItem *searchItem;
@property (nonatomic, retain) NSIndexPath *currentIndex;

@end
