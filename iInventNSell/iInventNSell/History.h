//
//  History.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UserProfile, SearchResults;

@interface History : NSManagedObject

@property (nonatomic, retain) NSNumber * historyCode;
@property (nonatomic, retain) NSDate * searchDate;
@property (nonatomic, retain) SearchResults *searchResults;
@property (nonatomic, retain) UserProfile *userProfile;

@end
