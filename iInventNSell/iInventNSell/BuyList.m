//
//  BuyList.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BuyList.h"
#import "Item.h"
#import "UserProfile.h"


@implementation BuyList

@dynamic buyListCode;
@dynamic creationDate;
@dynamic item;
@dynamic userProfile;

@end
