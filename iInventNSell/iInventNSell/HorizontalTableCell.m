//
//  HorizontalTableCell.m
//  iInventNSell
//
//  Created by Srikanth on 1/27/13.
//
//
#import "ControlVariables.h"
#import "HorizontalTableCell.h"
#import "ArticleCell_iPhone.h"
#import "SearchItem.h"
#import "HorizontalTableView.h"
#import "SearchResultsTableViewController.h"

@implementation HorizontalTableCell

@synthesize horizontalTableView;
@synthesize articles = _articles;
@synthesize searchResultsController;

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.articles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ArticleCell";
    
    ArticleCell_iPhone *cell = (ArticleCell_iPhone *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[ArticleCell_iPhone alloc] initWithFrame:CGRectMake(0, 0, kCellWidth, kCellHeight)] autorelease];
    }
    
	//NSDictionary *currentArticle = [self.articles objectAtIndex:indexPath.row];
    SearchItem *currentArticle = [self.articles objectAtIndex:indexPath.row];
    //cell.thumbnail.image = [UIImage imageNamed:[currentArticle objectForKey:@"ImageName"]];
    NSURL *url = currentArticle.imageURL;
	NSData *data = [NSData dataWithContentsOfURL:url];
	cell.thumbnail.image = [UIImage imageWithData:data];    
    cell.titleLabel.text = [currentArticle title];
    
    return cell;
}




#pragma mark - Memory Management

- (void)dealloc
{
    self.horizontalTableView = nil;
    self.articles = nil;
    
    [super dealloc];
}

- (NSString *) reuseIdentifier
{
    return @"HorizontalCell";
}

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
        self.horizontalTableView = [[[HorizontalTableView alloc] initWithFrame:CGRectMake(0, 0, kCellHeight, kTableLength)] autorelease];
        self.horizontalTableView.dataSource=self;
        self.horizontalTableView.showsVerticalScrollIndicator = NO;
        self.horizontalTableView.showsHorizontalScrollIndicator = NO;
        self.horizontalTableView.transform = CGAffineTransformMakeRotation(-M_PI * 0.5);
        [self.horizontalTableView setFrame:CGRectMake(kRowHorizontalPadding * 0.5, kRowVerticalPadding * 0.5, kTableLength - kRowHorizontalPadding, kCellHeight)];
        
        self.horizontalTableView.rowHeight = kCellWidth;
        self.horizontalTableView.backgroundColor = kHorizontalTableBackgroundColor;
        
        self.horizontalTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.horizontalTableView.separatorColor = [UIColor clearColor];
        
        self.horizontalTableView.dataSource = self;
        self.horizontalTableView.delegate = self;
        [self.horizontalTableView setArticles:self.articles];
        [self addSubview:self.horizontalTableView];
    }
    
    return self;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath horizontal table, %d", [indexPath row]);
    SearchItem *item = [self.articles objectAtIndex:indexPath.row];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"TabularView"
                                                             bundle: nil];
    
    ItemDetailsViewController *viewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ItemDetailsViewController"];
    [viewController setNavigatingFrom:@"ItemDetails"];
    [viewController setSearchItem:item];
    [[searchResultsController navigationController]pushViewController:viewController animated:YES];

    
}



@end