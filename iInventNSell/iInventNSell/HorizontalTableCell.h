//
//  HorizontalTableCell.h
//  iInventNSell
//
//  Created by Srikanth on 1/27/13.
//
//

#import <UIKit/UIKit.h>

@class SearchResultsTableViewController;
@class HorizontalTableView;

@interface HorizontalTableCell : UITableViewCell <UITableViewDelegate, UITableViewDataSource>
{
    HorizontalTableView *horizontalTableView;
    NSArray *_articles;
    SearchResultsTableViewController *searchResultsController;
}

@property (nonatomic, retain) HorizontalTableView *horizontalTableView;
@property (nonatomic, retain) NSArray *articles;
@property (nonatomic, retain) SearchResultsTableViewController *searchResultsController;

@end