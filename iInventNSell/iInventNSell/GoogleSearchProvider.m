//
//  GoogleSearchProvider.m
//  InvBox
//
//  Created by Srikanth on 3/11/13.
//
//

#import "GoogleSearchProvider.h"

@implementation GoogleSearchProvider

@synthesize searchResults;
@synthesize searchString;
@synthesize responseData;



NSString * const GOOGLE_SEARCH_URL=@"https://www.googleapis.com/shopping/search/v1/public/products?country=US&";
NSString * const GOOGLE_APP_KEY=@"AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA";

- (void)main {

    @autoreleasepool {
        // is this operation cancelled?
        if (self.isCancelled)
            return;
        if (searchString==nil)
        {
            NSLog(@"searchItem is null, returning");
            return;
        }
        
        NSLog(@"Organic search called with search Parameter: %@", searchString);
        NSString *url = GOOGLE_SEARCH_URL;
        if (self.isCancelled)
            return;
        url = [url stringByAppendingString:@"&q="];
        url = [url stringByAppendingString:searchString];
        url = [url stringByAppendingString:@"&key="];
        url = [url stringByAppendingString:GOOGLE_APP_KEY];
        NSLog(@"URL Constructed is %@", url);
        if (self.isCancelled)
            return;
        responseData = [[NSMutableData data] retain];
        searchResults = [NSMutableArray array];
        if (self.isCancelled)
            return;
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        [connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [connection start];
    }
}


/*
 Google
 url: "https://www.googleapis.com/shopping/search/v1/public/products?key=AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA&country=US&q=ipad";
 callname: products, params key, country=us and query string
 key:AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA
 
 
 */
- (void) initializeSearchRequest:(NSString *)searchValue andOrganicSearch:(OrganicSearch *) organicSearch
{
    self.searchString=searchValue;
    self.organicSearch = organicSearch;
}


#pragma mark NSURLConnection Delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Connection failed: %@", [error description]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    @try {
        
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        NSLog(responseString);
        NSLog(@"Connection did finish loading\n\n\n\n");
        //  NSDictionary *results = [responseString JSONValue];
        NSDictionary *response = [responseString JSONValue];
        NSMutableArray *results = [response objectForKey:@"items"];
        searchResults = [[NSMutableArray alloc]init];
        
        for (NSDictionary *item  in results)
        {
            SearchItem *searchItem = [[SearchItem alloc]init];
            NSDictionary *product = [item  objectForKey:@"product"];
            [searchItem setItemCode:[product objectForKey:@"googleId"]];
            [searchItem setTitle:[product objectForKey:@"title"]];
            [searchItem setDescription:[product objectForKey:@"description"]];
            [searchItem setConditionType:[product objectForKey:@"condition"]];
            NSArray *images = [product objectForKey:@"images"];
            NSDictionary *linkImage = [images objectAtIndex:0];
            NSString *stringurl = [linkImage objectForKey:@"link"];
            NSURL *url = [NSURL URLWithString:stringurl];
            [searchItem setImageURL:url];
            
            //[searchItem setCategoryName:<#(NSString *)#>];
            
            NSArray *inventories = [product objectForKey:@"inventories"];
            NSDictionary *inventory = [inventories objectAtIndex:0];
            [searchItem setAvailability:[inventory objectForKey:@"availability"]];
            [searchItem setCurrency:[inventory objectForKey:@"currency"]];
            [searchItem setPrice:[inventory objectForKey:@"price"]];
            [searchItem setSource:@"Google Product Search"];
            [searchResults addObject:searchItem];
            if ([searchResults count]==10) {
                break;
            }
        }
        NSLog(@"After setting search results total items are: %d" , [searchResults count]);
        [[self organicSearch] dataLoaded:searchResults];
    }
    @catch (NSException *exception) {
        NSLog(@"**********\n\nAn exception has occured\n\n******************" && [exception name]);
    }
    @finally {
        [connection release];
        [responseData release];
    }
}


@end
