//
//  BuyListTableViewController.h
//  iInventNSell
//
//  Created by user on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iInventNSellAppDelegate.h"
#import "DataController.h"
#import "Category.h"
#import "SearchItem.h"
#import "ItemDetailsViewController.h"
#import "ControlVariables.h"


@interface BuyListTableViewController : UITableViewController
{
NSMutableArray *items;
SearchItem *searchItem;
NSIndexPath *currentIndex;
}
- (IBAction)done:(id)sender;
@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, retain) SearchItem *searchItem;
@property (nonatomic, retain) NSIndexPath *currentIndex;

@end
