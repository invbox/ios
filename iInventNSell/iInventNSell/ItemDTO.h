//
//  ItemDTO.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Category.h"
#import "UserProfile.h"

@interface ItemDTO : NSObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * itemCode;
@property (nonatomic, retain) NSString * itemDescription;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSNumber * purchasePrice;
@property (nonatomic, retain) Category *category;
@property (nonatomic, retain) UserProfile *userProfile;
@property (nonatomic, retain) NSURL * imageURL;
@property (nonatomic, retain) NSString * conditionType;
@property (nonatomic, retain) NSString * source;
@property (nonatomic, retain) NSString * currency;
@property (nonatomic, retain) NSString * availability;
@property (nonatomic, retain) NSString * type;
@end
