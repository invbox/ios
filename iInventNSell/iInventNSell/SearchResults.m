//
//  SearchResults.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchResults.h"
#import "Item.h"
#import "MarketPlace.h"
#import "UserProfile.h"


@implementation SearchResults

@dynamic searchResultCode;
@dynamic listPrice;
@dynamic item;
@dynamic userProfile;

@end
