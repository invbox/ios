//
//  ItemDetailsViewController.h
//  iInventNSell
//
//  Created by user on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchItem.h"
#import "Item.h"

@interface ItemDetailsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UITextView *description;
@property (strong, nonatomic) IBOutlet UILabel  *availability;
@property (strong, nonatomic) SearchItem *searchItem;
@property (strong, nonatomic) NSString *navigatingFrom;
@property (strong, nonatomic) IBOutlet UIButton *addToInventory;
@property (strong, nonatomic) IBOutlet UIButton *sellButton;
@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UITextField *purchasePrice;
- (void)saveToServer:(SearchItem *)item forType:(NSString *)type;
- (IBAction)addToInventory:(id)sender;
- (void)addToHistory;
- (void)updateItem;
- (IBAction)edit:(id)sender;
- (IBAction)editingDone;
@end
