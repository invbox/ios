//
//  UserPreferences.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MarketPlace, UserProfile;

@interface UserPreferences : NSManagedObject

@property (nonatomic, retain) NSNumber * preferenceCode;
@property (nonatomic ) Boolean marketPlaceEnabled;
@property (nonatomic, retain) MarketPlace *marketplace;
@property (nonatomic, retain) UserProfile *userprofile;

@end
