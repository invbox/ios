//
//  UserProfile.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UserPreferences;

@interface UserProfile : NSManagedObject


@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * retainHistory;
@property (nonatomic, retain) NSNumber * userProfileCode;
@property (nonatomic, retain) UserPreferences *preferences;

@end
