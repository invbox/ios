//
//  MarketPlaceDTO.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MarketPlaceDTO.h"

@implementation MarketPlaceDTO

@synthesize marketPlaceCode;
@synthesize marketPlace;
@synthesize marketPlaceDescription;
@synthesize email;
@synthesize website;
@synthesize searchUrl;

@end
