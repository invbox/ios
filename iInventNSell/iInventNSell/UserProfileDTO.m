//
//  UserProfileDTO.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserProfileDTO.h"

@implementation UserProfileDTO

@synthesize userProfileCode;
@synthesize creationDate;
@synthesize email;
@synthesize firstName;
@synthesize lastName;
@synthesize retainHistory;
@synthesize preferences;

@end
