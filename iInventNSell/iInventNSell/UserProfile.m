//
//  UserProfile.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserProfile.h"


@implementation UserProfile


@dynamic creationDate;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic retainHistory;
@dynamic userProfileCode;
@dynamic preferences;

@end
