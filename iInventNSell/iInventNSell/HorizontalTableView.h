//
//  HorizontalTableView.h
//  InvBox
//
//  Created by Srikanth on 4/13/13.
//
//

#import <UIKit/UIKit.h>
#import "ItemDetailsViewController.h"
#import "SearchResultsTableViewController.h"
#import "SearchItem.h"

@interface HorizontalTableView : UITableView
{
    NSArray *_articles;
}

@property (nonatomic, retain) NSArray *articles;

@end
