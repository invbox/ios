//
//  eBaySearchProvider.h
//  InvBox
//
//  Created by Srikanth on 3/11/13.
//
//

#import <Foundation/Foundation.h>
#import "SearchItem.h"
@class OrganicSearch;

@interface eBaySearchProvider : NSOperation
{
    NSString *searchString;
    NSMutableArray *searchResults;
    NSMutableData *responseData;
}
@property (nonatomic, retain) NSMutableArray *searchResults;
@property (nonatomic, retain) NSString *searchString;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, retain) OrganicSearch *organicSearch;
extern NSString * const EBAY_SEARCH_URL;
extern NSString * const EBAY_APP_KEY;

- (void) initializeSearchRequest:(NSString *)searchValue andOrganicSearch:(OrganicSearch *)organicSearch;
@end
