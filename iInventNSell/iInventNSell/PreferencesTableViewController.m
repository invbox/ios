//
//  PreferencesTableViewController.m
//  iInventNSell
//
//  Created by user on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PreferencesTableViewController.h"


@implementation PreferencesTableViewController
@synthesize dataArray;
@synthesize searchRepositories;

- (IBAction)done:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage* background = [UIImage imageNamed:@"coppersulphatebackground.jpeg"];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:background]];

    
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController]; 
    
    NSMutableArray *data = [[NSMutableArray alloc] initWithArray:[dataController getAllRecordsFromDB:@"UserPreferences"]];
    NSMutableArray *repositories = [[NSMutableArray alloc] initWithArray:[dataController getAllRecordsFromDB:@"MarketPlace"]];
    //self.navigationItem.leftBarButtonItem.tintColor = [UIColor blueColor];
    self.navigationItem.leftBarButtonItem.tintColor = kNavigationBackgroundColor;
    //NSMutableArray *data = [[NSMutableArray alloc] initWithObjects:@"Amazon",@"eBay",@"BestBuy", @"Deals2Buy", @"Google Product Search", @"SearchUPC", nil];
    self.dataArray = data;
    self.searchRepositories = repositories;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    self.dataArray=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchRepositories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    NSUInteger row = [indexPath row];
    MarketPlace *marketPlace = [searchRepositories objectAtIndex:[indexPath row]];
    cell.textLabel.text = [marketPlace marketPlace];
    cell.tag = [[marketPlace marketPlaceCode]integerValue];
    NSLog(@"setting tag value to %d", cell.tag);
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    //cell.textLabel.text = [dataArray objectAtIndex:row];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"This is row %i" , row+1];
    UIImage *image;
    
    if (row==0)
    {
        image = [UIImage imageNamed:@"amazonicon.jpeg"];
    }
    else if (row==1)
    {
        image = [UIImage imageNamed:@"ebay.jpeg"];   
    }
    else if (row==2)
    {
        image = [UIImage imageNamed:@"bestbuyicon.jpeg"];   
    }    
    else if (row==3)
    {
        image = [UIImage imageNamed:@"deals2buyicon.jpeg"];   
    }    
    else if (row==4)
    {
        image = [UIImage imageNamed:@"googleprodsearchicon.jpeg"];   
    }
//    else if (row==5)
//    {
//        image = [UIImage imageNamed:@"searchupcicon.jpeg"];   
//    }
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    cell.imageView.image = imageView.image;
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    //cell.accessoryType = UITableViewCellAccessoryNone;
    UISwitch *mySwitch = [[[UISwitch alloc] init] autorelease];
    int repoMarketPlace = [[marketPlace marketPlaceCode]integerValue];
    int profile, preferencecode, marketplace;
    
    for (UserPreferences *preference in dataArray) {
    
        //marketplace = [[preference marketPlaceCode] intValue];
        //profile =  [[preference userProfileCode] intValue];
        preferencecode =  [[preference preferenceCode] intValue];
        /*if (marketplace == repoMarketPlace)
        {
            [mySwitch setOn:YES];
            break;
        }*/
    }
    
    [mySwitch addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
    cell.accessoryView = mySwitch;
    [imageView release];
    
    
    return cell;
}

- (IBAction)flip:(UISwitch *)sender {
    
    UITableViewCell *aCell = (UITableViewCell *)[sender superview]; 
    Boolean found = false;
    int marketplacecode, repomarketplacecode;
    UserPreferences *userPreferences;
    UserPreferencesDTO *prefs;
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController]; 
    
    NSLog(@"row selected text is : %@ at row: %d", aCell.textLabel.text, aCell.tag);
    repomarketplacecode = aCell.tag;
    
    for (UserPreferences *preference in dataArray) {
            
        marketplacecode = [[preference marketPlaceCode] intValue];
        if (marketplacecode == repomarketplacecode)
        {
            found=true;
            userPreferences = preference;
            break;
        }
    }
    
    if (found) {
        if (!sender.on) {
            NSLog(@"Removing object from array for id %d",[[userPreferences marketPlaceCode]intValue]);
            //[dataArray removeObject:userPreferences];
            [dataController deleteObject:userPreferences];
            //Write logic to remove the preference
        }
    }
    else {
        if (sender.on) {
            prefs = [[UserPreferencesDTO alloc]init];
            [prefs setUserProfileCode:[[NSNumber alloc] initWithInt:1]];
            [prefs setPreferenceCode:[[NSNumber alloc]initWithInt:1]];
            [prefs setMarketPlaceCode:[[NSNumber alloc]initWithInt:repomarketplacecode]];
            NSLog(@"Adding object to array for id %d",[[prefs marketPlaceCode]intValue]);
            [dataController createUserPreferences:prefs];
            [dataArray release];
            dataArray = [[NSMutableArray alloc] initWithArray:[dataController getAllRecordsFromDB:@"UserPreferences"]];
            [[self tableView]reloadData];
        }
        else {
            NSLog(@"\n ******* \n Does this case arise ! \n****** \n");
        }
    }
         
        
    
    

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Row selected %d", [indexPath row]);
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    NSUInteger row = [indexPath row];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    //NSLog("Accessory type=" );
    //NSLog(cell.accessoryType);
    if (cell.accessoryType==UITableViewCellAccessoryNone)
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    else if (cell.accessoryType==UITableViewCellAccessoryCheckmark)
            [cell setAccessoryType:UITableViewCellAccessoryNone];
    [tableView reloadData];

}

@end
