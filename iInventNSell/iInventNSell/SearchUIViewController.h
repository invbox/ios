//
//  SearchUIViewController.h
//  iInventNSell
//
//  Created by user on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataController.h"
#import "SearchResultsTableViewController.h"
#import "SearchItem.h"

@interface SearchUIViewController : UIViewController <ZBarReaderDelegate>
{
    UIButton *searchButton;
    UIImageView *resultImage;
    UITextField *searchText;
    UISearchBar *searchBar;

    NSMutableArray *searchItems;
    
    SearchResultsTableViewController *searchResults;
}

@property (nonatomic, retain) IBOutlet UIButton *searchButton;
@property (nonatomic, retain) IBOutlet UIImageView *resultImage;
@property (nonatomic, retain) IBOutlet UITextField *searchText;
@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic, retain) IBOutlet SearchResultsTableViewController *searchResults;
@property (nonatomic, retain) NSMutableArray *searchItems;
- (IBAction)search:(id)sender;
- (IBAction)scanButtonTapped;
- (IBAction)newCategory;
- (IBAction)editingDone;
- (IBAction)handleSearch:(UISearchBar *)searchBar;
@end
