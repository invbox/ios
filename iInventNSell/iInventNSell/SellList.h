//
//  SellList.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item, UserProfile;

@interface SellList : NSManagedObject

@property (nonatomic, retain) NSNumber * sellListCode;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) Item *item;
@property (nonatomic, retain) UserProfile *userProfile;

@end
