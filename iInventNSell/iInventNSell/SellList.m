//
//  SellList.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SellList.h"
#import "Item.h"
#import "UserProfile.h"


@implementation SellList

@dynamic sellListCode;
@dynamic creationDate;
@dynamic item;
@dynamic userProfile;

@end
