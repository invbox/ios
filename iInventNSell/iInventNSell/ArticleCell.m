//
//  ArticleCell.m
//  iInventNSell
//
//  Created by Srikanth on 2/23/13.
//
//

#import "ArticleCell.h"

@implementation ArticleCell

@synthesize thumbnail = _thumbnail;
@synthesize titleLabel = _titleLabel;

- (NSString *)reuseIdentifier
{
    return @"ArticleCell";
}

- (void)dealloc
{
    self.thumbnail = nil;
    self.titleLabel = nil;
    
    [super dealloc];
}

@end
