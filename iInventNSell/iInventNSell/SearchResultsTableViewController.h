//
//  SearchResultsTableViewController.h
//  iInventNSell
//
//  Created by user on 4/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iInventNSellAppDelegate.h"
#import "Category.h"
#import "DataController.h"
#import "SBJson.h"
#import "SearchItem.h"
#import "ItemDetailsViewController.h"
#import "HorizontalTableCell.h"
#import "ControlVariables.h"
#import "ArticleCell_iPhone.h"
#import "OrganicSearch.h"

@interface SearchResultsTableViewController : UITableViewController
{
    NSMutableArray *categories;	
    NSString *searchItem;
    NSMutableArray *searchResults;
    NSMutableData *responseData;
    UIActivityIndicatorView *activityIndicator;

    UIViewController *viewController;    
    UIBarButtonItem *editButton;
    UIBarButtonItem *cancelButton;
    UIBarButtonItem *deleteButton;
    UIBarButtonItem *addButton;

}
@property (nonatomic, retain) NSMutableArray *categories;
@property (nonatomic, retain) NSMutableArray *searchResults;
@property (nonatomic, retain) NSString *searchItem;
@property (nonatomic, readwrite) Boolean *resultsLoaded;
@property (nonatomic, retain) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, retain) IBOutlet UIViewController *viewController;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *editButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *cancelButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *deleteButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *addButton;

- (IBAction)done:(id)sender;
- (void) initializeSearchRequest;
- (void) reload;
- (IBAction)editAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)deleteAction:(id)sender;
- (IBAction)addAction:(id)sender;
- (UIView *) createHeader:(UIImage *)imageName;
@end
