//
//  MarketPlaceTableViewController.h
//  iInventNSell
//
//  Created by user on 9/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControlVariables.h"
//#import "PayPalMobile.h"

@interface MarketPlaceTableViewController : UITableViewController //<PayPalPaymentDelegate>

- (IBAction)checkOut:(id)sender;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
//@property(nonatomic, strong, readwrite) PayPalPayment *completedPayment;

@end
