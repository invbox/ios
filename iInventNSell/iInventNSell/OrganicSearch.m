//
//  OrganicSearch.m
//  InvBox
//
//  Created by Srikanth on 3/8/13.
//
//

#import "OrganicSearch.h"
#import "SearchResultsTableViewController.h"

@implementation OrganicSearch

@synthesize searchResults;
@synthesize searchString;
@synthesize responseData;
@synthesize searchResultsController;
@synthesize searchQueue;

NSInteger const  MAX_SEARCH_RESULTS=10;
NSString * const EBAY_SEARCH_STRING=@"http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&GLOBAL-ID=EBAY-US&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&paginationInput.entriesPerPage=10";
NSString * const EBAY_KEY=@"Cordysbdb-4c22-430f-9779-d37db46a34e";

/*ebay
 http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0
 &SECURITY-APPNAME=Cordysbdb-4c22-430f-9779-d37db46a34e
 &GLOBAL-ID=EBAY-US
 &RESPONSE-DATA-FORMAT=JSON
 &REST-PAYLOAD
 &keywords=harry%20potter
 &paginationInput.entriesPerPage=10
 

 
 appid: Cordysbdb-4c22-430f-9779-d37db46a34e
 url:http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0
 key:Cordysbdb-4c22-430f-9779-d37db46a34e
 callname: method invoking like FindItemsByKeyWords
 Search String: QueryKeywords, ex ipod
*/


/*
Google
 url: "https://www.googleapis.com/shopping/search/v1/public/products?key=AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA&country=US&q=ipad";
 callname: products, params key, country=us and query string
 key:AIzaSyCgP3jHASPl_1aie2YCb08yXuLyPDalMhA
 
 
*/
- (void) initializeSearchRequest:(NSString *)searchValue forTableView:(SearchResultsTableViewController *)searchController
{
    self.searchString=searchValue;
    self.searchResultsController = searchController;
    if (searchString==nil)
    {
        NSLog(@"searchItem is null, returning");
        return;
    }
        
    NSLog(@"Organic search called with search Parameter: %@", searchString);
    searchQueue = [[NSOperationQueue alloc] init];
    searchQueue.name = @"Search Requests Queue";
    /*
    eBaySearchProvider *eBaySearch = [[eBaySearchProvider alloc]init];
    [eBaySearch initializeSearchRequest:searchString andOrganicSearch:self];
    [searchQueue addOperation:eBaySearch];
    [eBaySearch release];
    */
    
    GoogleSearchProvider *googleSearch = [[GoogleSearchProvider alloc]init];
    [googleSearch initializeSearchRequest:searchString andOrganicSearch:self];
    [searchQueue addOperation:googleSearch];
    [googleSearch release];
}
    
- (void) dataLoaded:(NSMutableArray *) searchResults
{
    [searchResultsController setSearchResults:searchResults];
    [searchResultsController setResultsLoaded:TRUE];
    [[searchResultsController activityIndicator] stopAnimating];
    [[searchResultsController tableView] reloadData ];
    //[searchResultsController reload];
}

@end
