//
//  Category.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Category.h"
#import "Item.h"


@implementation Category

@dynamic categoryCode;
@dynamic categoryDescription;
@dynamic categoryName;
@dynamic creationDate;
@dynamic item;

@end
