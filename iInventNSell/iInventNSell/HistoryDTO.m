//
//  HistoryDTO.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HistoryDTO.h"

@implementation HistoryDTO

@synthesize historyCode;
@synthesize searchDate;
@synthesize searchResults;
@synthesize userProfile;

@end
