//
//  CategoryDTO.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"


@interface CategoryDTO : NSObject

@property (nonatomic, retain) NSNumber * categoryCode;
@property (nonatomic, retain) NSString * categoryDescription;
@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) Item *item;


@end
