//
//  UserPreferencesDTO.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserPreferencesDTO.h"

@implementation UserPreferencesDTO

@synthesize preferenceCode;
@synthesize marketPlaceEnabled;
@synthesize marketPlace;
@synthesize userProfile;

@end
