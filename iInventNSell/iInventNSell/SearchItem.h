//
//  SearchItem.h
//  iInventNSell
//
//  Created by user on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchItem : NSObject
@property (nonatomic, retain) NSString *objectId;
@property (nonatomic, retain) NSNumber * itemCode;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSURL * imageURL;
@property (nonatomic, retain) NSString * conditionType;
@property (nonatomic, retain) NSString * source;
@property (nonatomic, retain) NSString * description;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * currency;
@property (nonatomic, retain) NSString * availability;

@end
