//
//  MyProfileModalViewController.m
//  iInventNSell
//
//  Created by user on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyProfileModalViewController.h"

@implementation MyProfileModalViewController

@synthesize profileCode;
@synthesize firstName; 
@synthesize lastName;
@synthesize emailId; 
@synthesize retainHistory;
@synthesize lastModified;
@synthesize userProfileDTO;
@synthesize userProfile;
@synthesize textField;

- (IBAction)done:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)saveAndClose:(id)sender {
    userProfileDTO = [[UserProfileDTO alloc]init];
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController]; 
    NSLog(@"first name: %@", firstName.text);
    NSLog(@"last name: %@", lastName.text);
    NSLog(@"email id: %@", emailId.text);
    [userProfileDTO setFirstName:firstName.text];
    [userProfileDTO setLastName:lastName.text];
    [userProfileDTO setEmail:emailId.text];
    if (retainHistory.on)
        [userProfileDTO setRetainHistory:[[NSNumber alloc]initWithInt:1]];
    else
        [userProfileDTO setRetainHistory:[[NSNumber alloc]initWithInt:0]];
    [userProfileDTO setCreationDate:[NSDate date]];
    NSLog(@"firstname: %@", [userProfileDTO firstName]);
    NSLog(@"lastname: %@", [userProfileDTO lastName]);
    NSLog(@"email: %@", [userProfileDTO email]);    
    [dataController updateUser:userProfileDTO forUserID:1];
//    [[UIAlertView alloc]initWithTitle:@"Profile updated" message:@"Profile successfully updated" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [self dismissModalViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    UIImage* background = [UIImage imageNamed:@"coppersulphatebackground.jpeg"];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:background]];

    
    /*
    self.navigationController.navigationBar.tintColor = kHeadingBackgroundColor;
    self.tabBarController.tabBar.tintColor = kHeadingBackgroundColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor];
    [self.tabBarItem  setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
    */
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController]; 
    
    NSArray  *users = [dataController getAllRecordsFromDB:@"UserProfile"];
    userProfile = [users objectAtIndex: 0];
    NSNumber *userProfileCode = [userProfile userProfileCode];
    NSLog(@"profile code: %d", [userProfileCode intValue]);
    //profileCode.text = [NSString stringWithFormat:@"%d",[[userProfile userProfileCode] intValue]]; 
    profileCode.text = [NSString stringWithFormat:@"%d",[userProfile userProfileCode]];
    NSLog(@"First name: %@", [userProfile firstName]); 
    NSLog(@"Last name: %@", [userProfile lastName]);
    NSLog(@"Email Id: %@", [userProfile email]);
    NSLog(@"History: %@", [userProfile retainHistory]);
    firstName.text = [userProfile firstName];
    lastName.text = [userProfile lastName];
    emailId.text = [userProfile email];
    if ([[userProfile retainHistory] intValue]==1)
        [retainHistory setOn:YES];
    else
        [retainHistory setOn:NO];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"d-M-yyyy h:mm.ss a"];
    NSString  *strlastmodified = [dateFormatter stringFromDate: [userProfile creationDate]];
    NSLog(@"Lastmodified: d/M/YYYY", strlastmodified);
    lastModified.text = strlastmodified;
    self.navigationController.navigationBar.tintColor = [UIColor blueColor];
    //self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}

 

- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    [self setTextField: textField];
}

- (IBAction)dismissKeyboard:(id)sender;
{
    [textField resignFirstResponder];
}

@end
