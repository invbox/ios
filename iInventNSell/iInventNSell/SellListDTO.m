//
//  SellListDTO.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SellListDTO.h"

@implementation SellListDTO

@synthesize sellListCode;
@synthesize creationDate;
@synthesize item;
@synthesize userProfile;

@end
