//
//  SearchUIViewController.m
//  iInventNSell
//
//  Created by user on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchUIViewController.h"
#import "iInventNSellAppDelegate.h"
#import "SBJson.h"
#import "ControlVariables.h"

@implementation SearchUIViewController
@synthesize searchButton;
@synthesize resultImage;
@synthesize searchText;
@synthesize searchResults;
@synthesize searchBar;
@synthesize searchItems;

- (IBAction)editingDone {
    [searchText resignFirstResponder];
}

- (IBAction)search:(id)sender
{
    NSLog(@"\n\nSearch clicked\n\n");
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"searchResults"])
    {
        searchResults  = [segue destinationViewController];
        NSString *escapedString = (NSString *)CFURLCreateStringByAddingPercentEscapes( NULL,	 (CFStringRef)searchBar.text,	 NULL,	 (CFStringRef)@"!’\"();:@&=+$,/?%#[]% ", kCFStringEncodingISOLatin1);
        [searchResults setSearchItem:escapedString];
        [escapedString release];
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self handleSearch:searchBar];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    //[self handleSearch:searchBar];
}

- (void)handleSearch:(UISearchBar *)searchBar {
    NSLog(@"handle search User searched for %@", searchBar.text);
    [searchBar resignFirstResponder]; // if you want the keyboard to go away
    //[self prepareForSegue:[self searchResults] sender:self];
    [self performSegueWithIdentifier: @"searchResults" sender: self];
    
    
    
     
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
    [self performSegueWithIdentifier: @"searchResults" sender: self];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    NSLog(@"cancelbutton clicked User canceled search");
    [searchBar resignFirstResponder]; // if you want the keyboard to go away
}

- (IBAction)newCategory {
    NSLog(@"new category action called");
    
   /* iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController]; 
    NSString *categoryName = [searchText text];
    categoryName = @"Garments";
    [dataController createNewCategoryWithName:categoryName andDescription:@"Category dummy description"];
    NSArray *categories = [dataController getAllCategoriesFromDB];
  	[searchResults setCategories:[[NSMutableArray alloc] initWithArray: categories]];*/

    NSLog(@"New category creation done");
}

- (IBAction)scanButtonTapped {
    NSLog(@"Scan Button Tapped");
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    
    [scanner setSymbology:ZBAR_I25 config:ZBAR_CFG_ENABLE to:0];
    [self presentModalViewController:reader animated:YES];
    [reader release];
}

- (void) imagePickerController:(UIImagePickerController *)reader didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for (symbol in results)
        searchText.text = symbol.data;
    resultImage.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [reader dismissModalViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Search";
    UIImage* background = [UIImage imageNamed:@"coppersulphatebackground.jpeg"];

    //self.navigationController.view.backgroundColor = [UIColor colorWithPatternImage:background];
    self.searchBar.backgroundImage = background;
    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:background]];
    [[UITabBar appearance] setBackgroundImage:background];
    
   /*
    self.navigationController.navigationBar.tintColor = kHeadingBackgroundColor;
    self.tabBarController.tabBar.tintColor = kHeadingBackgroundColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor];
    [self.tabBarItem  setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
     
    */
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}

- (void) dealloc
{
    self.resultImage=nil;
    self.searchText = nil;
    [super dealloc];
}
@end
