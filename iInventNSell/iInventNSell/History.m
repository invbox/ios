//
//  History.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "History.h"
#import "UserProfile.h"


@implementation History

@dynamic historyCode;
@dynamic searchDate;
@dynamic searchResults;
@dynamic userProfile;

@end
