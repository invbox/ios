//
//  UserPreferences.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserPreferences.h"
#import "MarketPlace.h"
#import "UserProfile.h"


@implementation UserPreferences

@dynamic preferenceCode;
@dynamic marketPlaceEnabled;
@dynamic marketplace;
@dynamic userprofile;

@end
