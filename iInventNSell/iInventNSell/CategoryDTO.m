//
//  CategoryDTO.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CategoryDTO.h"

@implementation CategoryDTO

@synthesize categoryCode;
@synthesize categoryDescription;
@synthesize categoryName;
@synthesize creationDate;
@synthesize item;

@end
