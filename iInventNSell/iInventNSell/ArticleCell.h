//
//  ArticleCell.h
//  iInventNSell
//
//  Created by Srikanth on 2/23/13.
//
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell
{
    UIImageView *_thumbnail;
    UILabel *_titleLabel;
}


@property (nonatomic, retain) UIImageView *thumbnail;
@property (nonatomic, retain) UILabel *titleLabel;

@end
