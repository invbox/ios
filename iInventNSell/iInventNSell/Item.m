//
//  Item.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Item.h"
#import "Category.h"
#import "UserProfile.h"


@implementation Item


@dynamic creationDate;
@dynamic itemCode;
@dynamic itemDescription;
@dynamic itemName;
@dynamic purchasePrice;
@dynamic category;
@dynamic userProfile;
@dynamic  imageURL;
@dynamic conditionType;
@dynamic  source;
@dynamic  currency;
@dynamic  availability;
@dynamic  type;
@end
