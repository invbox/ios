//
//  SearchItem.m
//  iInventNSell
//
//  Created by user on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchItem.h"

@implementation SearchItem
@synthesize objectId;
@synthesize itemCode;
@synthesize title;
@synthesize categoryName;
@synthesize imageURL;
@synthesize conditionType;
@synthesize source;
@synthesize description;
@synthesize price;
@synthesize currency;
@synthesize availability;
@end
