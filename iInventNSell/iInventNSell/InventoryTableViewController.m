//
//  InventoryTableViewController.m
//  iInventNSell
//
//  Created by user on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InventoryTableViewController.h"


@implementation InventoryTableViewController
@synthesize items;
@synthesize searchItem;
@synthesize currentIndex;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


/* 
 Use this to bring the tableview into edit mode
 - (void)setEditing:(BOOL)editing animated:(BOOL)animated
 Use this to handle the delete event 
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 */
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    /*
    self.navigationController.navigationBar.tintColor = kHeadingBackgroundColor;
    self.tabBarController.tabBar.tintColor = kHeadingBackgroundColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor];
    [self.tabBarItem  setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
    */
    iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    DataController *dataController = [appDelegate dataController]; 
    //[dataController createNewItemWithName:@"Nokia E7-00" andDescription:@"Nokia Business Mobile"];
    NSArray  *itms = [dataController getAllRecordsFromDB:@"Item"];
    [self setItems:[[NSMutableArray alloc] initWithArray: itms]];
        
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    UIImage* background = [UIImage imageNamed:@"coppersulphatebackground.jpeg"];
    
    self.navigationItem.leftBarButtonItem.tintColor = kNavigationBackgroundColor; //[UIColor blueColor];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar setBackgroundImage:background forBarMetrics:UIBarMetricsDefault];
    [[self view] setBackgroundColor:[UIColor colorWithPatternImage:background]];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"deleting row %d", [indexPath row]);
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: @"Delete" 
                              message: @"Do you really want to delete?" 
                              delegate: self
                              cancelButtonTitle: @"Ok"
                              otherButtonTitles: @"Cancel", nil];
        [alert show];
        [alert release];
    }

    self.currentIndex = indexPath;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{

    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
        Item *item = [items objectAtIndex: [self.currentIndex row]];
        iInventNSellAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        DataController *dataController = [appDelegate dataController]; 
        [dataController deleteItem:[[[item objectID] URIRepresentation] absoluteString]];
        [items removeObjectAtIndex: currentIndex.row];
        [[self tableView] deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndex] withRowAnimation:YES];
    }
    else
    {
        NSLog(@"do nothing");
    }
              
}
              
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    // Configure the cell...
    Item *item = [items objectAtIndex: [indexPath row]];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellEditingStyleInsert;
    [cell.textLabel setText: [item itemName]];
	[cell.detailTextLabel setText: [item itemDescription]];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    NSURL *url = item.imageURL;
	NSData *data = [NSData dataWithContentsOfURL:url];
	cell.imageView.image = [UIImage imageWithData:data];
    

    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    searchItem = [SearchItem alloc];
    Item *item = [items objectAtIndex: [indexPath row]];
    searchItem.objectId = [[[item objectID] URIRepresentation] absoluteString];
    searchItem.title = item.itemName;
    searchItem.description = item.itemDescription;
    searchItem.categoryName = @"Electronics";
    searchItem.price = item.purchasePrice;
    searchItem.availability = @"Yes";
    searchItem.currency = item.currency;
    searchItem.availability = item.availability;
    searchItem.imageURL = item.imageURL;
    searchItem.source = item.source;
    
    NSLog(@"title: %@", [item itemName]);
    NSLog(@"description: %@" , [item description]);
    NSLog(@"URL : %@", [item imageURL] );
    
    
        //self.viewController.title = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
        ItemDetailsViewController *viewController = [[self storyboard]instantiateViewControllerWithIdentifier:@"ItemDetailsViewController"];
        [viewController setNavigatingFrom:@"Inventory"];
        [viewController setSearchItem:searchItem];
        [[self navigationController] pushViewController:viewController animated:YES];
        //[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
