//
//  MarketPlace.m
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MarketPlace.h"


@implementation MarketPlace

@dynamic marketPlaceCode;
@dynamic marketPlace;
@dynamic marketPlaceDescription;
@dynamic email;
@dynamic website;
@dynamic searchUrl;

@end
