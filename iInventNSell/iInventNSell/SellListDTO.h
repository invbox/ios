//
//  SellListDTO.h
//  iInventNSell
//
//  Created by user on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemDTO.h"
#import "UserProfile.h"

@interface SellListDTO : NSObject


@property (nonatomic, retain) NSNumber * sellListCode;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) ItemDTO *item;
@property (nonatomic, retain) UserProfile *userProfile;



@end
