//
//  InventoryTableViewController.h
//  iInventNSell
//
//  Created by user on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"
#import "iInventNSellAppDelegate.h"
#import "DataController.h"
#import "Category.h"
#import "SearchItem.h"
#import "ItemDetailsViewController.h"
#import "ControlVariables.h"

@interface InventoryTableViewController : UITableViewController
{
    NSMutableArray *items;	
	SearchItem *searchItem;
    NSIndexPath *currentIndex;
}
@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, retain) SearchItem *searchItem;
@property (nonatomic, retain) NSIndexPath *currentIndex;
@end
