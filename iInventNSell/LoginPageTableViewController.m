    //
//  LoginPageTableViewController.m
//  iInventNSell
//
//  Created by user on 7/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginPageTableViewController.h"

@implementation LoginPageTableViewController
@synthesize emailText;
@synthesize passwordText;

- (IBAction)login {
    NSString * userid;
    NSString * pwd;
    userid = emailText.text;
    pwd = passwordText.text;
    if ([userid length]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Login" message:@"Emailid cannot be blank!" delegate:nil
                              cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    if ([pwd length]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Login" message:@"Password cannot be blank!" delegate:nil
                              cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    NSLog(@"User id: %@", userid);
    NSLog(@"Password: %@", pwd);
    NSString *storedpwd = [KeychainUtil getStringForKey:userid];
//    NSString *storedpwd = [KeychainUtil getStringForKey:@"password"];
    NSLog(@"stored password :%@", storedpwd);

    if (storedpwd!=nil)
    {
        if (![storedpwd isEqualToString:pwd])
        {
            //NSLog(@"Incorrect password, type in the correct password");
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Login Failed" message:@"Incorrect password!" delegate:nil
                                  cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        else
        {
            [self performSegueWithIdentifier:@"TabBarSegue" sender:self];
        }
    }
    else
    {
        [KeychainUtil saveString:pwd forKey:userid];        
        [self performSegueWithIdentifier:@"TabBarSegue" sender:self];
    }
    [KeychainUtil saveString:emailText.text forKey:@"invboxuser"];
    
}
- (IBAction)editingDone {
    //[emailText resignFirstResponder];
    [self.view.window endEditing:TRUE];
}

- (IBAction)passwordEditDone {
    //[passwordText resignFirstResponder];
    [self.view endEditing:TRUE];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return TRUE;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIImage* background = [UIImage imageNamed:@"coppersulphatebackground.jpeg"];
    self.tableView.backgroundView = [[UIImageView alloc]initWithImage:background];
    //self.tableView.separatorColor = [UIColor whiteColor];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    /*UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10, 190, self.view.bounds.size.width-20, 1)];
    lineView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:lineView];
    [lineView release];*/
    NSString *invBoxUser = [KeychainUtil getStringForKey:@"invboxuser"];
    emailText.text = invBoxUser;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 640, 150)];
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(10, 3, tableView.bounds.size.width - 10, 18)] autorelease];
    if (section==1)
        label.text = @" Login";
    else
        label.text = @"";
    label.textColor = [UIColor whiteColor];
    [label setFont:[UIFont fontWithName:@"TrebuchetMS-Bold" size:18]];
    label.backgroundColor = [UIColor clearColor];
    [header addSubview:label];
    return header;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/*
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/




@end
