//
//  LoginPageTableViewController.h
//  iInventNSell
//
//  Created by user on 7/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeychainUtil.h"
#import "SearchUIViewController.h"

@interface LoginPageTableViewController : UITableViewController
{
    UITextField *emailText;
    UITextField *passwordText;
}
@property (nonatomic, retain) IBOutlet UITextField *emailText;
@property (nonatomic, retain) IBOutlet UITextField *passwordText;

- (IBAction)editingDone;
- (IBAction)passwordEditDone;
- (IBAction) login;
@end
